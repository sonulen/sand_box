/*
 * obsh.h
 *
 *  Created on: Apr 26, 2018
 *      Author: sonulen
 */

#ifndef SW_INFRASTRUCTURE_OBSH_H_
#define SW_INFRASTRUCTURE_OBSH_H_

// Для объявления шаблонного класса с реализацией в .cpp файле можно
// сделать вот так и подключать generic.h
// и не нужно будет явное создание экземпляров шаблона
#include "test_template.h"
#include "test_template.cpp"


#endif /* SW_INFRASTRUCTURE_OBSH_H_ */
